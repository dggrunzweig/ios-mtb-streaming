//
//  ViewController.m
//  streaming-mtb-player
//
//  Created by David Grunzweig on 11/30/16.
//  Copyright © 2016 Dysonics. All rights reserved.
//


#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    motionSensor = new iPhoneMotionSensor();
    DSMotionDeviceManager::get()->setDefaultDevice(motionSensor);
    decoder.streamAudioFromURL("http://s3.amazonaws.com/com.dysonics.rondo.demos/mtb_wheresbob.mp4");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
