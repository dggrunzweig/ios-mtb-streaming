//
//  AppDelegate.h
//  streaming-mtb-player
//
//  Created by David Grunzweig on 11/30/16.
//  Copyright © 2016 Dysonics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

