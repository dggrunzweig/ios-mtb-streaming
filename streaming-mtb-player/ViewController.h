//
//  ViewController.h
//  streaming-mtb-player
//
//  Created by David Grunzweig on 11/30/16.
//  Copyright © 2016 Dysonics. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "ffmpegFileDecoder.h"
#include "iPhoneMotionSensor.h"

@interface ViewController : UIViewController

{
    ffmpegFileDecoder decoder;
    iPhoneMotionSensor* motionSensor;
}
@end

