//
//  ffmpegDecoder.h
//  streaming-mtb-player
//
//  Created by David Grunzweig on 11/30/16.
//  Copyright © 2016 Dysonics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

extern NSString * decoderErrorDomain;

typedef enum {
    decoderErrorNone,
    decoderErrorOpenFile,
    decoderErrorStreamInfoNotFound,
    decoderErrorStreamNotFound,
    decoderErrorCodecNotFound,
    decoderErrorOpenCodec,
    decoderErrorAllocateFrame,
    decoderErrorSetupScaler,
    decoderErrorReSampler,
    decoderErrorUnsupported,
} decoderError;

typedef enum {
    
    frameTypeAudio,
    frameTypeVideo,
    frameTypeArtwork,
    frameTypeSubtitle,
    
} decoderFrameType;

typedef enum {
    
    frameFormatRGB,
    frameFormatYUV,
    
} decoderFrameFormat;

@interface decoderAbstractFrame : NSObject
@property (readonly, nonatomic) decoderFrameType type;
@property (readonly, nonatomic) CGFloat position;
@property (readonly, nonatomic) CGFloat duration;
@end

@interface decoderAudioFrame : decoderAbstractFrame
@property (readonly, nonatomic, strong) NSData *samples;
@end

@interface decoderVideoFrame : decoderAbstractFrame
@property (readonly, nonatomic) decoderFrameFormat format;
@property (readonly, nonatomic) NSUInteger width;
@property (readonly, nonatomic) NSUInteger height;
@end

@interface decoderVideoFrameRGB : decoderVideoFrame
@property (readonly, nonatomic) NSUInteger linesize;
@property (readonly, nonatomic, strong) NSData *rgb;
- (UIImage *) asImage;
@end

@interface decoderVideoFrameYUV : decoderVideoFrame
@property (readonly, nonatomic, strong) NSData *luma;
@property (readonly, nonatomic, strong) NSData *chromaB;
@property (readonly, nonatomic, strong) NSData *chromaR;
@end

@interface decoderArtworkFrame : decoderAbstractFrame
@property (readonly, nonatomic, strong) NSData *picture;
- (UIImage *) asImage;
@end

//typedef BOOL(^ffmpegDecoderInterruptCallback)();

@interface ffmpegDecoder : NSObject

@property (readonly, nonatomic, strong) NSString *path;
@property (readonly, nonatomic) BOOL isEOF;
@property (readwrite,nonatomic) CGFloat position;
@property (readonly, nonatomic) CGFloat duration;
@property (readonly, nonatomic) CGFloat fps;
@property (readonly, nonatomic) CGFloat sampleRate;
@property (readonly, nonatomic) NSUInteger frameWidth;
@property (readonly, nonatomic) NSUInteger frameHeight;
@property (readonly, nonatomic) NSUInteger audioStreamsCount;
@property (readwrite,nonatomic) NSInteger selectedAudioStream;
@property (readonly, nonatomic) BOOL validVideo;
@property (readonly, nonatomic) BOOL validAudio;
@property (readonly, nonatomic, strong) NSDictionary *info;
@property (readonly, nonatomic, strong) NSString *videoStreamFormatName;
@property (readonly, nonatomic) BOOL isNetwork;
@property (readonly, nonatomic) CGFloat startTime;
@property (readwrite, nonatomic) BOOL disableDeinterlacing;
//@property (readwrite, nonatomic, strong) ffmpegDecoderInterruptCallback interruptCallback;

+ (id) movieDecoderWithContentPath: (NSString *) path
                             error: (NSError **) perror;

- (BOOL) openFile: (NSString *) path
            error: (NSError **) perror;

-(void) closeFile;

- (BOOL) setupVideoFrameFormat: (decoderFrameFormat) format;

- (NSArray *) decodeFrames: (CGFloat) minDuration;

@end

