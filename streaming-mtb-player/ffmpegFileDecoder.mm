//
//  ffmpegFileDecoder.cpp
//  streaming-mtb-player
//
//  Created by David Grunzweig on 12/6/16.
//  Copyright © 2016 Dysonics. All rights reserved.
//

#include "ffmpegFileDecoder.h"
#include <Accelerate/Accelerate.h>

#include <math.h>

static void ffmpegCallback(float* buffer, UInt32 numFrames, UInt32 timeStamp, UInt32 flags, void* context)
{
    ffmpegFileDecoder* SELF = (ffmpegFileDecoder*)context;
    if (SELF->shouldRead())
    {
        SELF->readFromQueueBuffer(buffer, numFrames);
        SELF->decodeMore();
    }
    else
    {
        int numChannels = SELF->mInfo.numChannels;
        for (int i = 0; i < numChannels; ++i)
        {
            for (int j = 0; j < numFrames; ++j)
            {
                buffer[i + j*numChannels] = 0;
            }
        }
    }
}


ffmpegFileDecoder::ffmpegFileDecoder()// : hasData(false)
{
    
}

ffmpegFileDecoder::~ffmpegFileDecoder()
{
}


void ffmpegFileDecoder::streamAudioFromURL(const char *filename)
{
    setName("FFMPEG STREAM");
    
    avcodec_register_all();
    av_register_all();
    
    hasData = false;
    
    AVCodec* codec;
    mFormatContext = setupFormatContextForStream(filename, &codec);
    
    createCodecContextsForStream(mFormatContext, codec, &mInfo, &mAudioContexts, &mVideoContexts);
    
    for (int i = 0; i < mAudioContexts.size(); ++i)
    {
        //if the sample format isn't float interleaved or planar (fltp)
        //or if the samplerate isn't 44100
        //need to create conversion audio contexts and convert the stream
        SwrContext* resample_context;
        init_resampler(mAudioContexts[i], mAudioContexts[i]->channel_layout, 44100, AV_SAMPLE_FMT_FLTP, &resample_context);
        mConvertedAudioContexts.push_back(resample_context);
    }
    
    //get total size of file to create buffer
    int totalChannels = 0;
    int numsamples = 0;
    for (int i = 0; i < mFormatContext->nb_streams; ++i)
    {
        totalChannels = totalChannels+mFormatContext->streams[i]->codecpar->channels;
        numsamples = (int)mFormatContext->streams[i]->duration;
        //        printf("totalChannels: %i, numSamples: %i\n", totalChannels, numsamples);
    }
    
    mTotalFramesInFile = numsamples;
    mRemainingFramesInFile = numsamples; //keep track of total file length
    
    //use 1000 buffers of 1024 for now (approx 23 seconds)
    mQueueBuffer.setSize(totalChannels, 1000*1024);
    mQueueBufferLength = 1000*1024;
    
    DSAudioEngine::initialize(mInfo.sampleRate, 512);
    DSAudioEngine::get()->start();
    mInfo.format = DSAudioFormatLinearPCM;
    mInfo.bitsPerSample = 32;
    mInfo.numChannels = totalChannels;
    
    //start the callback
    mStreamedAudioTrack = DSSound::create((DSSoundSourceCallback)ffmpegCallback, mInfo, this);
    
    //start the thread and decode
    start();
    mStreamedAudioTrack->play();

}

int ffmpegFileDecoder::run()
{
    //start pulling down the file
    decode_mtb_stream(mFormatContext, mAudioContexts, mConvertedAudioContexts, &mInfo, &mQueueBuffer);
    
    return 0;
}


AVFormatContext* ffmpegFileDecoder::setupFormatContextForStream(const char *filename, AVCodec** codec)
{
    avformat_network_init();
    
    AVFormatContext* formatContext = NULL;
    int error;
    //set format context data based on contents of file
    error = avformat_open_input(&formatContext, filename, nullptr, nullptr);
    if (error < 0)
    {
        fprintf(stderr, "Could not open %s\n", filename);
        printFFmpegError(error);
    }
    
    //get info about for format context based on header of streams
    error = avformat_find_stream_info(formatContext, nullptr);
    if (error < 0)
    {
        printFFmpegError(error);
    }
    
    av_find_best_stream(formatContext, AVMEDIA_TYPE_AUDIO, -1, -1, codec, NULL);
    
    //print off info about the stream
    av_dump_format(formatContext, 0, filename, 0);
    
    return formatContext;
}

void ffmpegFileDecoder::init_resampler(AVCodecContext* input_context, uint64_t channelLayoutOutput, int sampleRateOut, AVSampleFormat samplefmt, SwrContext** resample_context)
{
    int error = 0;
    *resample_context = swr_alloc_set_opts(NULL, channelLayoutOutput, samplefmt, sampleRateOut, input_context->channel_layout, input_context->sample_fmt, input_context->sample_rate, 0, NULL);
    
    error = swr_init(*resample_context);
    if (error < 0)
    {
        printf("Could not init resample context\n");
        printFFmpegError(error);
    }
}

void ffmpegFileDecoder::createCodecContextsForStream(AVFormatContext* formatContext, AVCodec* codec, DSAudioInfo* info, std::vector<AVCodecContext*>* audioContexts, std::vector<AVCodecContext*>* videoContexts)
{
    int error = 0;
    for (int i = 0; i < formatContext->nb_streams; ++i)
    {
        if (formatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
        {
            //sets the codec information to the information of the "best stream"
            AVCodecContext* context = NULL;//avcodec_alloc_context3(codec);
            //            if (!context) {
            //                fprintf(stderr, "Could not allocate audio codec context\n");
            //            }
            
            info->sampleRate = formatContext->streams[i]->codecpar->sample_rate;
            //create contexts for audio streams
            context = formatContext->streams[i]->codec;
            if (av_sample_fmt_is_planar(context->sample_fmt) == 1)
            {
                info->flags = DSAudioFlagsIsNonInterleaved;
            }
            else
            {
                info->flags = DSAudioFlagsNone;
            }
            
            /* open it */
            error = avcodec_open2(context, codec, NULL);
            if (error < 0)
            {
                fprintf(stderr, "Could not open codec\n");
                printFFmpegError(error);
            }
            
            audioContexts->push_back(context);
        }
        else if (formatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            //create contexts for video streams
            
        }
        else
        {
            //subtitle or artwork
            
        }
    }
}

void ffmpegFileDecoder::convertSampleToOutputFormat(AVFrame* inputFrame, AVFrame* outputFrame, SwrContext* resampleContext)
{
    int error = 0;
    const uint8_t** inputData = (const uint8_t**)inputFrame->data;
    uint8_t** outputData;
    for (int i = 0; i < outputFrame->channels; ++i)
    {
        outputData = &(outputFrame->data[i]);
        error = swr_convert(resampleContext, outputData, (int)outputFrame->nb_samples, inputData, (int)inputFrame->nb_samples);
    }
    if (error < 0)
    {
        printf("Could not convert samples\n");
        printFFmpegError(error);
    }
}

void ffmpegFileDecoder::decode_mtb_stream(AVFormatContext* formatContext, std::vector<AVCodecContext*> audioContexts, std::vector<SwrContext*> conversionContexts, DSAudioInfo* info, DSArray2D<float>* outputFIFOBuffer)
{
    //map the mtb files to an ordered 8 channel file in this section so that all we have to do is interlace in the callback
    const UInt32 MTB_CHANNEL_MAPPING_8[4][2] = { { 6, 2 }, { 7, 3 }, { 5, 1 }, { 4, 0 } };
    
    AVPacket avpkt;
    //initialize packet for streaming
    av_init_packet(&avpkt);
    
    mQueueWriteIndex = 1;
    mQueueReadIndex = 0;
    mTotalNumberOfDecodedFrames = 0;
    
    //set data on the AVFrame
    AVFrame* inputFrame;
    inputFrame = av_frame_alloc();
    AVFrame* outputFrame;
    outputFrame = av_frame_alloc();
    
    int error = 0;
    hasData = false;
    /* decode until eof */
    bool eof = false;
    while (!eof)
    {
        if (remainingWriteSpace() < 1024)
        {
            //don't keep writing data to buffer if there isn't space left
            //wait for read here
            hasData = true;
            mReadEvent.wait();
            continue;
        }
        else if (remainingWriteSpace() < .5*mQueueBufferLength)
        {
            //if the writing buffer is more than half full, let the callback start playing
            //but dont stop the decoder
            hasData = true;
        }
        else
        {
            //stop playing and let it decode only
            hasData = false;
        }
        
        avpkt.data = NULL;
        avpkt.size = 0;
        //        avcodec_flush_buffers(codecContext);
        avformat_flush(formatContext);
        av_packet_unref(&avpkt);
        av_frame_unref(inputFrame);
        av_frame_unref(outputFrame);
        error = av_read_frame(formatContext, &avpkt);
        if (error == 0)
        {
            int streamID = avpkt.stream_index;
            av_grow_packet(&avpkt, AV_INPUT_BUFFER_PADDING_SIZE);
            error = avcodec_send_packet(audioContexts[streamID], &avpkt);
            if (error == 0)
            {
                error = avcodec_receive_frame(audioContexts[streamID], inputFrame);
                if (error == 0)
                {
                    int numFrames = (int)(inputFrame->nb_samples);
                    int channels = inputFrame->channels;
                    outputFrame = av_frame_clone(inputFrame);
                    av_frame_set_sample_rate(outputFrame, 44100);
                    av_frame_set_channel_layout(outputFrame, inputFrame->channel_layout);
                    for (int i = 0; i < channels; ++i)
                    {
                        int bufferChannel;
                        if (info->numChannels == 8)
                            bufferChannel = MTB_CHANNEL_MAPPING_8[streamID][i];
                        else
                            bufferChannel = i;
                        
                        //convert
                        convertSampleToOutputFormat(inputFrame, outputFrame, conversionContexts[streamID]);
                        for (int j = 0; j < numFrames; ++j)
                        {
                            mQueueBuffer[bufferChannel][(mQueueWriteIndex+j)%mQueueBufferLength] = ((float*)outputFrame->data[i])[j];
                        }
                    }
                    if (streamID == (formatContext->nb_streams - 1))
                    {
                        mQueueWriteIndex = (mQueueWriteIndex + numFrames) % mQueueBufferLength;
                        mTotalNumberOfDecodedFrames+=numFrames;
                    }
                }
                else
                {
                    printf("Recieve Frame Error\n");
                    printFFmpegError(error);
                }
            }
            else
            {
                printf("Send Packet Error\n");
                printFFmpegError(error);
            }
            
        }
        else if (error == AVERROR_EOF)
        {
            eof = true;
        }
        else
        {
            printf("Read Frame Error\n");
            printFFmpegError(error);
        }
    }
    
    for (int i = 0; i < audioContexts.size(); ++i)
    {
        avcodec_close(audioContexts[i]);
        av_free(audioContexts[i]);
    }
    av_frame_free(&inputFrame);
    av_frame_free(&outputFrame);
}

void ffmpegFileDecoder::decodeMore()
{
    mReadEvent.signal();
}

void ffmpegFileDecoder::updateReadIndex(int byFrames)
{
    mQueueReadIndex = (mQueueReadIndex + byFrames) % mQueueBufferLength;
    mRemainingFramesInFile = mRemainingFramesInFile - byFrames;
    if (mRemainingFramesInFile <= 0)
    {
        mRemainingFramesInFile = 0;
        hasData = false;
    }
    printf("Percent Played: %f Percent Loaded: %f\n", (float)mRemainingFramesInFile / mTotalFramesInFile, (float)mTotalNumberOfDecodedFrames / mTotalFramesInFile);
}

void ffmpegFileDecoder::play()
{
    mStreamedAudioTrack->play();
}

bool ffmpegFileDecoder::isPlaying()
{
    return (mStreamedAudioTrack->getState() == DSPlaybackStatePlaying);
}

void ffmpegFileDecoder::pause()
{
    mStreamedAudioTrack->pause();
}

int ffmpegFileDecoder::getTotalFileSizeInFrames()
{
    return mTotalFramesInFile;
}

int ffmpegFileDecoder::getNumberOfUnplayedFrames()
{
    return mRemainingFramesInFile;
}

int ffmpegFileDecoder::getTotalNumberOfDecodedFrames()
{
    return mTotalNumberOfDecodedFrames;
}

bool ffmpegFileDecoder::shouldRead()
{
    return hasData;
}

int ffmpegFileDecoder::remainingWriteSpace()
{
    int spacing = 0;
    spacing = mQueueReadIndex - mQueueWriteIndex;
    if (spacing < 0)
    {
        spacing = mQueueBufferLength + spacing;
    }
    return spacing;
}

void ffmpegFileDecoder::readFromQueueBuffer(float *buffer, int numFrames)
{
    int numChannels = mInfo.numChannels;
    for (int i = 0; i < numChannels; ++i)
    {
        for (int j = 0; j < numFrames; ++j)
        {
            buffer[i + j*numChannels] = mQueueBuffer[i][(j+mQueueReadIndex)%mQueueBufferLength];
            //reset the data just read to 0;
            mQueueBuffer[i][(j+mQueueReadIndex)%mQueueBufferLength] = 0;
        }
    }
    updateReadIndex(numFrames);
}

void ffmpegFileDecoder::printFFmpegError(int error)
{
    printf("AVError %i: %s\n", error, av_err2str(error));
}
