//
//  ffmpegFileDecoder.hpp
//  streaming-mtb-player
//
//  Created by David Grunzweig on 12/6/16.
//  Copyright © 2016 Dysonics. All rights reserved.
//

#ifndef ffmpegFileDecoder_h
#define ffmpegFileDecoder_h
#import <Foundation/Foundation.h>
extern "C"
{
#include "libavutil/opt.h"
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libavutil/channel_layout.h"
#include "libavutil/common.h"
#include "libavutil/imgutils.h"
#include "libavutil/mathematics.h"
#include "libavutil/samplefmt.h"
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
}
#include "rondo.h"
#include "DSThread.h"
#include "DSWaitableEvent.h"

class ffmpegFileDecoder : public DSThread
{
public:
    ffmpegFileDecoder();
    virtual ~ffmpegFileDecoder();
    
    void streamAudioFromURL(const char* filename);
    DSAudioInfo mInfo;
    void readFromQueueBuffer(float* buffer, int numFrames);
    int getTotalFileSizeInFrames();
    int getNumberOfUnplayedFrames();
    int getTotalNumberOfDecodedFrames();
    void play();
    bool isPlaying();
    void pause();
    bool shouldRead();
    void decodeMore();
    //inherited from Thread
    virtual int run();

private:
    AVFormatContext* setupFormatContextForStream(const char *filename, AVCodec** codec);
    void createCodecContextsForStream(AVFormatContext* formatContext, AVCodec* codec, DSAudioInfo* info, std::vector<AVCodecContext*>* audioContexts, std::vector<AVCodecContext*>* videoContexts);
    void init_resampler(AVCodecContext* input_context, uint64_t channelLayoutOutput, int sampleRateOut, AVSampleFormat samplefmt, SwrContext** resample_context);
    void convertSampleToOutputFormat(AVFrame* inputFrame, AVFrame* outputFrame, SwrContext* resampleContext);
    void decode_mtb_stream(AVFormatContext* formatContext, std::vector<AVCodecContext*> audioContexts, std::vector<SwrContext*> conversionContexts, DSAudioInfo* info, DSArray2D<float>* outputFIFOBuffer);
    void updateReadIndex(int byFrames);
    void printFFmpegError(int error);
    
    DSSoundRef mStreamedAudioTrack;
    int mQueueWriteIndex;
    std::vector<AVCodecContext*> mAudioContexts;
    std::vector<SwrContext*> mConvertedAudioContexts;
    std::vector<AVCodecContext*> mVideoContexts;
    AVFormatContext* mFormatContext;
    bool hasData;
    int mTotalNumberOfDecodedFrames;
    int mQueueReadIndex;
    int mQueueBufferLength;
    DSArray2D<float> mQueueBuffer;
    int mRemainingFramesInFile;
    int mTotalFramesInFile;
    int remainingWriteSpace();
    DSWaitableEvent mReadEvent;
};

#endif /* ffmpegFileDecoder_hpp */
